﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

public class XMLManager : Singleton<XMLManager>
{
    public Leaderboard leaderboard;
    

    [System.Serializable]
    public class Leaderboard
    {
        public int[] list;
        public string[] nameList;
    }

    public void SaveScore(int[] scoresToSave, string[] namesToSave)
    {
        //if (!Directory.Exists(Application.persistentDataPath + "/HighScores/highscores.xml"))
        //{
        //    Directory.CreateDirectory(Application.persistentDataPath + "/HighScores/highscores.xml");
        //}
        leaderboard.list = scoresToSave;
        leaderboard.nameList = namesToSave;
        //XmlSerializer serializer = new XmlSerializer(typeof(Leaderboard));
        //TextWriter stream = new StreamWriter(Application.persistentDataPath + "/HighScores/highscores.xml");
        ////FileStream stream = new FileStream(Application.persistentDataPath + "/HighScores/highscores.xml", FileMode.OpenOrCreate);
        //serializer.Serialize(stream, leaderboard);        
        //stream.Close();
        if(PlayerPrefs.GetInt("HighScore0") == 0)
        {
            PlayerPrefs.SetInt("HighScore0", scoresToSave[0]);
            PlayerPrefs.SetString("HighScoreName0", namesToSave[0]);
        }
        else
        {
            for (int i = 0; i < scoresToSave.Length - 1; i++)
            {
                PlayerPrefs.SetInt("HighScore" + i.ToString(), scoresToSave[i]);
                PlayerPrefs.SetString("HighScoreName" + i.ToString(), namesToSave[0]);
            }
        }

    }

    public void SubmitHighScore(int highScore = 1000, string myName = "ABC")
    {
        //if (highScores == null)
        // highScores = new int[].Length = highScoresCount;

        //Load All HighScores (Up to 5)
        //List<int> loadedScores = new List<int>();
        int[] loadedScores = { 0, 0, 0, 0, 0 };
        for (int i = 0; i < 5; i++)
        {
            if (PlayerPrefs.GetInt("HighScore" + i.ToString()) != 0)
            {
                //loadedScores.Add(PlayerPrefs.GetInt("HighScore" + i.ToString()));
                loadedScores[i] = PlayerPrefs.GetInt("HighScore" + i.ToString());
            }
            else
            {
                //loadedScores.Add(0);
            }
        }
        //Load all names
        //List<string> loadedNames = new List<string>();
        string[] loadedNames = { "", "", "", "", "" };
        for (int i = 0; i < 5; i++)
        {
            if (PlayerPrefs.GetString("HighScoreName" + i.ToString()) != "")
            {
                //loadedNames.Add(PlayerPrefs.GetString("HighScoreName" + i.ToString()));
                loadedNames[i] = PlayerPrefs.GetString("HighScoreName" + i.ToString());
            }
            else
            {
                //loadedNames.Add("");
            }
        }

        //Find where the score sits inside list
        int newHighScoreIndex = -1;
        for (int i = 0; i < loadedScores.Length - 1; i++)
        {
            if (highScore >= loadedScores[i])
            {
                newHighScoreIndex = i;
                break;
            }
        }
        //if score does sit inside the list
        if (newHighScoreIndex >= 0)
        {
            for (int i = loadedScores.Length - 1; i > newHighScoreIndex; i--)
            {
                
                //loadedScores.Insert(i - 1, loadedScores[i]);
                //loadedNames.Insert(i - 1, loadedNames[i]);
                loadedScores[i] = loadedScores[i - 1];
                loadedNames[i] = loadedNames[i - 1];
            }
            loadedScores[newHighScoreIndex] = highScore;
            loadedNames[newHighScoreIndex] = myName;
        }

        leaderboard.list = loadedScores;
        leaderboard.nameList = loadedNames;

        //Save Scores
        for (int i = 0; i < 5 - 1; i++)
        {
            if(loadedScores[i] != 0)
            {
                PlayerPrefs.SetInt("HighScore" + i.ToString(), loadedScores[i]);
                PlayerPrefs.SetString("HighScoreName" + i.ToString(), loadedNames[i]);
            }
        }
    }

    public int[] LoadScores()
    {

        //if (File.Exists(Application.persistentDataPath + "HighScores/highscores.xml"))
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(Leaderboard));
        //    FileStream stream = new FileStream(Application.persistentDataPath + "/HighScores/highscores.xml", FileMode.Open);
        //    leaderboard = serializer.Deserialize(stream) as Leaderboard;
        //    stream.Close();

        //}
            return leaderboard.list;
        
        //return loadedScores.ToArray();
    }

    public string[] LoadNames()
    {
        //if (File.Exists(Application.persistentDataPath + "HighScores/highscores.xml"))
        //{
        //    XmlSerializer serializer = new XmlSerializer(typeof(Leaderboard));
        //    FileStream stream = new FileStream(Application.persistentDataPath + "/HighScores/highscores.xml", FileMode.Open);
        //    leaderboard = serializer.Deserialize(stream) as Leaderboard;

        //}

        return leaderboard.nameList;

        
        //return loadedNames.ToArray();
    }
}
