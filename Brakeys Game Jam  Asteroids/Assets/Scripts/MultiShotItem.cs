﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiShotItem : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if(transform.position.y <= -4)
        {
            GameManager.Instance.UpdateMultiShot();
            Destroy(gameObject);
        }
    }
}
