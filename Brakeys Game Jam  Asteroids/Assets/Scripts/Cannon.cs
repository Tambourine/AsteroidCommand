﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [Header("Variables")]
    [SerializeField]
    private float maxAngle;
    public int ammo;
    [SerializeField]
    private int maxAmmo;
    [SerializeField]
    private float ammoRefilTimer;
    [SerializeField]
    private float maxRefilTime; 

    [Header("GameObjects")]
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private GameObject barrelBack;
    [SerializeField]
    private GameObject barrelFront;
    [SerializeField]
    private float upImpulse;
    [SerializeField]
    private float sideImpulse;
    [SerializeField]
    private float torque;
    [SerializeField]
    private Transform doubleShotTrans; 

    private Rigidbody2D[] RB2Ds;
    private Vector3 obj;
    private float angle;

    private void OnEnable()
    {
        RB2Ds = GetComponentsInChildren<Rigidbody2D>();
        ammoRefilTimer = maxRefilTime;
        RefilAmmo(6);
    }
    private void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = -10;
        obj = Camera.main.WorldToScreenPoint(barrelFront.transform.position);
        mousePos.x = mousePos.x - obj.x;
        mousePos.y = mousePos.y - obj.y;
        angle = (Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg) ;

        if (angle - 90 >= -maxAngle && angle - 90 <= maxAngle)
        {
            barrelFront.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
            barrelBack.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
        }
        else if (angle - 90 < -maxAngle)
        {
            
            barrelBack.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -maxAngle));
            barrelFront.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -maxAngle ));
        }
        else
        {
            barrelBack.transform.rotation = Quaternion.Euler(new Vector3(0, 0, maxAngle));
            barrelFront.transform.rotation = Quaternion.Euler(new Vector3(0, 0, maxAngle));
        }

        if (GameManager.Instance.playingGame == true)
        {
            ammoRefilTimer -= Time.deltaTime;
            if (ammoRefilTimer <= 0)
            {
                ammoRefilTimer = maxRefilTime;
                RefilAmmo(1);

            }
        }
    }
    public void RefilAmmo(int refilAmount)
    {
        if(refilAmount + ammo > maxAmmo)
        {
            int tot = refilAmount + ammo;
            int remainder = tot % maxAmmo;
            refilAmount -= remainder;
            if (refilAmount <= 0)
                return;
        }

        if (ammo <= maxAmmo)
        {
            ammo += refilAmount;
            UIManager.Instance.UpdateAmmo(this.name, refilAmount);
        }
    }

    public void Shoot(Vector3 target)
    {
        if (ammo > 0)
        {
            //Update UI 
            RefilAmmo(-1);

            switch (GameManager.Instance.currentMultiShots)
            {
                case 1:
                    
                        ShootGun(target, transform.position);
                    
                    break;
                case 2:
                    
                        ShootGun(target, transform.position);
                        ShootGun(target - new Vector3(-0.5f, 0, 0), doubleShotTrans.position);
                    
                    break;
                case 3:
                    
                        ShootGun(target, transform.position);
                        ShootGun(target - new Vector3(-0.5f, 0, 0), doubleShotTrans.position);
                        ShootGun(target - new Vector3(-2, 0, 0), transform.position);
                    
                    break;
                case 4:
                    
                        ShootGun(target, transform.position);
                        ShootGun(target - new Vector3(-0.5f, 0, 0), doubleShotTrans.position);
                        ShootGun(target - new Vector3(-2, 0, 0), transform.position);
                        ShootGun(target - new Vector3(+2, 0, 0), transform.position);
                    
                    break;
            }
        }
    }

    void ShootGun(Vector3 target, Vector3 from)
    {
        //play sounds
        AudioController.Instance.PlaySfx("Shoot");

        GameObject thisbullet = Instantiate(bullet, from, Quaternion.identity);
        //getbullet componet and give it the blow up spot
        thisbullet.GetComponent<CannonBullet>().target = target;
    }
    public void KillThis()
    {
        for (int i = 0; i < RB2Ds.Length; i++)
        {
            RB2Ds[i].bodyType = RigidbodyType2D.Dynamic;
            RB2Ds[i].AddForce(new Vector2(Random.Range(-sideImpulse, sideImpulse), Random.Range(3, upImpulse)), ForceMode2D.Impulse);
            RB2Ds[i].AddTorque(torque,ForceMode2D.Impulse);
        }
        GetComponent<BoxCollider2D>().enabled = false; 
        AudioController.Instance.PlaySfx("CannonDeath");
        UIManager.Instance.UpdateAmmo(this.name, 0);
        GameManager.Instance.DestroyedCannon(this.gameObject);
    }
}
