﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private float deathTimer;
    ParticleSystem pSystem;
    // Start is called before the first frame update
    void Start()
    {
        pSystem = GetComponent<ParticleSystem>();
        deathTimer = pSystem.main.startLifetime.constant;
        Destroy(gameObject, deathTimer); 
    }
}
