﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plane : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed;
    [SerializeField]
    private GameObject negativeItem;
    [SerializeField]
    private GameObject destroyedPlane;
    [SerializeField]
    private float sideForce;
    [SerializeField]
    private float torque; 
    [SerializeField]
    private Vector3 target;

    bool hit = false; 
    private void Start()
    {
        //if we are on the left side of the screen
        if(this.transform.position.x < -5)
        {   //move right 
            target = this.transform.position + new Vector3(30, 0, 0);
        }
        else //we are on the right side of the room 
        {   // move left
            GetComponent<SpriteRenderer>().flipX = true; 
            target = this.transform.position + new Vector3(-30, 0, 0);
        }

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(this.transform.position, target, Time.deltaTime * moveSpeed);

        if (transform.position == target)
            Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.tag == "Bullet" && hit == false)
        {
            hit = true;
            Instantiate(negativeItem, transform.position, Quaternion.identity);
            KillThis();
        }
    }

    void KillThis()
    {
        AudioController.Instance.PlaySfx("PlaneDeath");
        GameObject pieces = Instantiate(destroyedPlane, transform.position, Quaternion.identity);
        foreach(Rigidbody2D rb in pieces.GetComponentsInChildren<Rigidbody2D>())
        {
            //if(GetComponent<SpriteRenderer>().flipX == true)
            //{
            //    rb.GetComponentInChildren<SpriteRenderer>().flipX = true; 
            //}
            rb.AddForce(Vector2.right * Random.Range(-sideForce, sideForce), ForceMode2D.Impulse);
            rb.AddTorque(Random.Range(180,torque));
        }
        Destroy(gameObject); 
    }
}
