﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBullet : MonoBehaviour
{
    public Vector3 target;

    [SerializeField]
    private Transform thisSprite; 
    [SerializeField]
    private GameObject explosion; 
    [SerializeField]
    private float bulletSpeed;
    [SerializeField]
    private float rotateSpeed; 


    Rigidbody2D RB; 
    // Start is called before the first frame update
    void Start()
    {
        transform.LookAt(target);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(this.transform.position, target, Time.deltaTime * bulletSpeed) ;


        if(Mathf.Approximately(transform.position.x, target.x ))
        {
            AudioController.Instance.PlaySfx("BulletExplosion");
            ExplodeThis();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Asteroid")
        {
            ExplodeThis();
        }
    }

    void ExplodeThis()
    {
        Instantiate(explosion, this.transform.position, Quaternion.identity);
        
        Destroy(gameObject);
    }
}
