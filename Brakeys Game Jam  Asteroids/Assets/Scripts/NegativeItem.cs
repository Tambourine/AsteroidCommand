﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NegativeItem : MonoBehaviour
{
    void Update()
    {
        if (transform.position.y <= -4)
        {

            GameManager.Instance.NegativeItem();
            Destroy(gameObject);
        }
    }
}
