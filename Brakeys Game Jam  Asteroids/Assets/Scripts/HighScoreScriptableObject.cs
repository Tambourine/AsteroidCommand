﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ScriptableObjects/HighScoreManager", order = 1)]
public class HighScoreScriptableObject : ScriptableObject
{
    public int[] highScores = { 0, 0, 0, 0, 0 }; 
    public string[] names; 
    public int[] score;

    [SerializeField]
    private int highScoresCount = 5;
    public int HighScoresCount
    {
        get { return highScoresCount; }
    }

    public void SubmitHighScore(int highScore = 1000, string myName = "ABC")
    {
        //if (highScores == null)
           // highScores = new int[].Length = highScoresCount;

        int newHighScoreIndex = -1;
        for (int i = 0; i < highScores.Length; i++)
        {
            if (highScore >= highScores[i])
            {
                newHighScoreIndex = i;
                break;
            }
        }
        if (newHighScoreIndex >= 0)
        {
            for (int i = highScores.Length - 1; i > newHighScoreIndex; i--)
            {
                highScores[i] = highScores[i - 1];
                names[i] = names[i - 1];
            }
            highScores[newHighScoreIndex] = highScore;
            names[newHighScoreIndex] = myName; 
        }
    }
}
