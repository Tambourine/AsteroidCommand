﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadItem : MonoBehaviour
{
    [SerializeField]
    private int refilAmount; 
    bool itemRecieved = false;
    // Update is called once per frame
    void Update()
    {
        if (transform.position.y <= -4 && itemRecieved == false)
        {
            itemRecieved = true;
            for (int i = 0; i < GameManager.Instance.cannons.Count; i++)
            {
                GameManager.Instance.cannons[i].GetComponent<Cannon>().RefilAmmo(refilAmount);
            }
            Destroy(gameObject);
        }
    }
}


