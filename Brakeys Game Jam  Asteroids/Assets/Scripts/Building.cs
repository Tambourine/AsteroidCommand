﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    [SerializeField]
    private GameObject destroyedBuilding;
    [SerializeField]
    private Transform destroyedBuildingSpawn; 
    [SerializeField]
    private float upForce;
    [SerializeField]
    private float sideForce;
    [SerializeField]
    private float torque;

    private SpriteRenderer spriteRend;
    private BoxCollider2D col; 
    private void Start()
    {
        spriteRend = GetComponentInChildren<SpriteRenderer>();
        col = GetComponent<BoxCollider2D>();
    }
    public void KillThis()
    {
        spriteRend.enabled = false;
        col.enabled = false; 
        GameObject building = Instantiate(destroyedBuilding, destroyedBuildingSpawn); 
        foreach(Rigidbody2D rb in building.GetComponentsInChildren<Rigidbody2D>())
        {
            rb.AddForce(new Vector2(Random.Range(-sideForce, sideForce), Random.Range(3, upForce)), ForceMode2D.Impulse);
            rb.AddTorque(torque);
        }

        AudioController.Instance.PlaySfx("BuildingDeath");
        GameManager.Instance.DestroyedBuilding(this.gameObject);
    }
}
