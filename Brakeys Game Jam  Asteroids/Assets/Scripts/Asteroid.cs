﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField]
    private float force;
    [SerializeField]
    private float maxRandomImpulse; 
    [SerializeField]
    private float rotationSpeed; 
    public Vector2 target;
    [SerializeField]
    private float maxScale;
    [SerializeField]
    private float minScale; 

    [Header("Sprites")]
    [SerializeField]
    private Sprite currentSprite; 
    [SerializeField]
    private Sprite[] sprites;

    [Header("Particles")]
    [SerializeField]
    private GameObject explosionParticles;
    [SerializeField]
    private ParticleSystem trailParticles;

    private ParticleSystem.EmissionModule trailEmission;
    private bool isVisible = false; 
    private List<GameObject> targetsAlive = new List<GameObject>(); 
    private Rigidbody2D RB; 
    private SpriteRenderer rend;
    private CircleCollider2D col;
    private bool hit = false; 
    // Start is called before the first frame update
    void Start()
    {
        RB = GetComponent<Rigidbody2D>();
        rend = GetComponentInChildren<SpriteRenderer>();
        col = GetComponent<CircleCollider2D>();
        trailEmission = trailParticles.emission;

        //Sprites
        currentSprite = sprites[Random.Range(0, sprites.Length)];
        rend.sprite = currentSprite;
        rotationSpeed = Random.Range(0.5f, rotationSpeed);
        //size
        float newScale = Random.Range(minScale, maxScale);
        transform.localScale = new Vector3(newScale,newScale,newScale);

        if (GameManager.Instance.playingGame)
        {
            //get spots to attack from gamemanager
            targetsAlive.AddRange(GameManager.Instance.buildings);
            targetsAlive.AddRange(GameManager.Instance.cannons);

            //choose a spot to aim towards 
            target = targetsAlive[Random.Range(0, targetsAlive.Count)].transform.position;
            //based on our pos, add force towards target based on x distance 
            float xDist = target.x - transform.position.x;
            maxRandomImpulse = GameManager.Instance.score * 0.005f + maxRandomImpulse;
            Vector2 Downforce = new Vector2(Random.Range(0, maxRandomImpulse), 0);

            RB.AddForce(Vector2.right * xDist * force, ForceMode2D.Impulse);
            RB.AddForce(Downforce);
        }
    }

    // Update is called once per frame
    void Update()
    {        

        rend.gameObject.transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);

        if(transform.position.y <= -7)
        {
            Destroy(gameObject);
        }
    }

    public void KillThis()
    {
        if (hit == true)
            return;

        AudioController.Instance.PlaySfx("AsteroidExplosion", Random.Range(0.5f,1.2f));

        explosionParticles.SetActive(true);
        trailEmission.rateOverTime = 0;

        RB.velocity = Vector2.zero;
        RB.angularVelocity = 0;
        
        col.enabled = false; 
        rend.enabled = false; 

        GameManager.Instance.UpdateScore();
        UIManager.Instance.UpdateUIScore();

        GameManager.Instance.DropItem(transform.position);

        Destroy(gameObject,2);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Building" || collision.tag == "Cannon")
        {
            hit = true;
            collision.gameObject.SendMessage("KillThis");
            Destroy(gameObject);
        }

        if (collision.tag == "Bullet")
        {
            KillThis();
            hit = true;
        }

    }
}
