﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
public class AudioController : Singleton<AudioController>
{
    [Header("Variables")]
    [SerializeField]
    private bool sfxOn = true;
    [SerializeField]
    private bool musicOn = true;

    [Header("UI")]
    [SerializeField]
    private Color offColor;
    [SerializeField]
    private Color onColor; 

    [Header("-----Sounds--------")]
    [SerializeField]
    private Sounds[] sfx;

    [SerializeField]
    private Sounds[] music; 
    // Start is called before the first frame update
    void OnEnable()
    {
        foreach (Sounds s in sfx)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.loop = s.loop;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }

        foreach (Sounds s in music)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.loop = s.loop;
            s.source.volume = s.volume;
        }

    }
    public void PlayMusic(string name)
    {
        if (musicOn)
        {
            Sounds s = Array.Find(music, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogError("Music: " + name + " not found!");
                return;
            }
            s.source.Play();
        }
    }
    public void PlaySfx(string name, float pitch = 1)
    {
        if (sfxOn)
        {
            Sounds s = Array.Find(sfx, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogError("Music: " + name + " not found!");
                return;
            }
            if(pitch != 1)
                s.source.pitch = pitch; 
            s.source.Play();
        }
    }

    public void ToggleSfx(TextMeshProUGUI thisText)
    {
        sfxOn = !sfxOn;
        PlaySfx("DoubleClick");
        if (thisText.text == "ON")
        {
            thisText.text = "OFF";
            thisText.color = offColor; 
        }
        else
        {
            thisText.text = "ON";
            thisText.color = onColor;
        }

    }

    public void ToggleMusic(TextMeshProUGUI thisText)
    {
        musicOn = !musicOn;
        PlaySfx("DoubleClick");
        if (thisText.text == "ON")
        {
            music[0].source.Stop();
            thisText.text = "OFF";
            thisText.color = offColor;
        }
        else
        {
            music[0].source.Play();
            thisText.text = "ON";
            thisText.color = onColor;
        }
    }

    public void UpdateSoundToggles(TextMeshProUGUI sfxToggle, TextMeshProUGUI musicToggle)
    {
        if (musicOn == false)
        {
            musicToggle.text = "OFF";
            musicToggle.color = offColor;
        }
        else
        {
            musicToggle.text = "ON";
            musicToggle.color = onColor;
        }

        if (sfxOn == false)
        {
            sfxToggle.text = "OFF";
            sfxToggle.color = offColor;
        }
        else
        {
            sfxToggle.text = "ON";
            sfxToggle.color = onColor;
        }

    }
}
