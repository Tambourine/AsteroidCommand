﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
public class UIManager : Singleton<UIManager>
{
    [Header("In Game Items")]
    [SerializeField]
    private GameObject gameScreen;
    [SerializeField]
    private TextMeshProUGUI scoreTxt;
    [SerializeField]
    private TextMeshProUGUI[] ammoText;

    [Header("Pause Screen")]
    [SerializeField]
    private TextMeshProUGUI pauseMusicToggle;
    [SerializeField]
    private TextMeshProUGUI pauseSfxToggle; 
    [SerializeField]
    private GameObject playPauseScreen;
    [SerializeField]
    private GameObject playPauseButton;
    [SerializeField]
    private GameObject rUSureGO;
    [SerializeField]
    private TextMeshProUGUI restartTxt; 

    [Header("Loose Screen")]
    [SerializeField]
    private GameObject loseScreen;
    [SerializeField]
    private TextMeshProUGUI loseScore;
    [SerializeField]
    private TMP_InputField nameInput; 

    [Header("Main Menu Items")]
    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private float flashSpeed;
    private bool highScoreRecorded = false; 

    [Header("High Scores")]
    [SerializeField]
    private HighScoreScriptableObject highScoreSO;
    [SerializeField]
    private TextMeshProUGUI[] highscores; 

    public List<Canvas> canvas = new List<Canvas>();
    private UnityEvent clickSoundEvent = new UnityEvent(); 
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    // Start is called before the first frame update
    void Start()
    {
        loseScore.text = "0";
        loseScreen.SetActive(false);
        mainMenu.SetActive(true);
        playPauseScreen.SetActive(false);
        playPauseButton.SetActive(false);
        canvas.AddRange( GetComponentsInChildren<Canvas>());
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        for (int i = 0; i < canvas.Count; i++)
        {
            canvas[i].worldCamera = Camera.main;
        }

        for (int i = 0; i < ammoText.Length; i++)
        {
            ammoText[i].gameObject.SetActive(true);
        }


    }
    private void Update()
    {
        if (loseScreen.activeSelf)
        {
            if (nameInput.characterLimit == nameInput.text.Length && highScoreRecorded == false)
            {                
                highScoreRecorded = true;
                AudioController.Instance.PlaySfx("DoubleClick");
                nameInput.readOnly = true;
                //Highscores
                XMLManager.Instance.SubmitHighScore(GameManager.Instance.score, nameInput.text);

                int[] newHighScores = XMLManager.Instance.leaderboard.list;
                string[] newNames = XMLManager.Instance.leaderboard.nameList;
                for (int i = 0; i < 5 - 1; i++)
                {
                    if (newHighScores[i] == 0 && newNames[i] == "")
                    {
                        highscores[i].text = "";                        
                    }
                    else
                        highscores[i].text = newNames[i] + "   " + newHighScores[i].ToString();
                }

                
            }
        }

    }

    public void UpdateUIScore()
    {
        scoreTxt.text = "Score: " + GameManager.Instance.score.ToString(); 
    }

    public void UpdateAmmo(string cannonName, int refilAmount)
    {
        if (refilAmount > 0)
        {
            for (int i = 0; i < refilAmount; i++)
            {
                if (cannonName.Contains("Left"))
                {
                    ammoText[0].text += "i";

                }
                else if (cannonName.Contains("Mid"))
                {
                    ammoText[1].text += "i";
                }
                else
                {
                    ammoText[2].text += "i";
                }
            }
        }else
        {
            for (int i = 0; i < -refilAmount; i++)
            {
                if (cannonName.Contains("Left"))
                {
                    int totAmmo = ammoText[0].text.Length;
                    if(totAmmo != 0)
                        ammoText[0].text = ammoText[0].text.Remove(totAmmo-1);

                }
                else if (cannonName.Contains("Mid"))
                {
                    int totAmmo = ammoText[1].text.Length;
                    if (totAmmo != 0)
                        ammoText[1].text = ammoText[1].text.Remove(totAmmo-1);
                }
                else
                {
                    int totAmmo = ammoText[2].text.Length;
                    if (totAmmo != 0)
                        ammoText[2].text = ammoText[2].text.Remove(totAmmo - 1);
                }
            }

        }
        
        if(refilAmount == 0)
        {
            if (cannonName.Contains("Left"))
            {
                ammoText[0].gameObject.SetActive(false);

            }
            else if (cannonName.Contains("Mid"))
            {
                ammoText[1].gameObject.SetActive(false);
            }
            else
            {
                ammoText[2].gameObject.SetActive(false);
            }
        }
    }

    public void PausePlay(TextMeshProUGUI myText)
    {
        AudioController.Instance.PlaySfx("DoubleClick");
        Debug.Log("Pause/Play");
        //We are pausing
        if(Time.timeScale == 1)
        {
            GameManager.Instance.SetCursor(false);
            Time.timeScale = 0;
            playPauseScreen.SetActive(true);
            myText.text = ">";
            restartTxt.GetComponent<TextMeshProUGUI>().text = "Restart?";
            rUSureGO.SetActive(false);

            //Play Add
            GoogleMobileAdsScript.Instance.RequestBanner();
        }
        else
        {
            GameManager.Instance.SetCursor(true);
            Time.timeScale = 1;
            playPauseScreen.SetActive(false);
            myText.text = "II";


            //Kill Add
            GoogleMobileAdsScript.Instance.DestroyBanner();
        }

        AudioController.Instance.UpdateSoundToggles(pauseSfxToggle, pauseMusicToggle);

    }

    public void PauseRestartButton()
    {
        AudioController.Instance.PlaySfx("Click");
        if(restartTxt.text == "Restart?")
        {
            restartTxt.text = "Are You Sure?";
            rUSureGO.SetActive(true); 
        }
        else
        {
            restartTxt.text = "Restart?";
            rUSureGO.SetActive(false);
        }
    }

    public void ConfirmRestart()
    {
        AudioController.Instance.PlaySfx("DoubleClick");
        RestartGame();
    }

    public void PlayGame()
    {
        Debug.Log("Playing Game");
        AudioController.Instance.PlaySfx("DoubleClick");
        playPauseScreen.SetActive(false);
        mainMenu.SetActive(false);
        gameScreen.SetActive(true);
        playPauseButton.SetActive(true);
        GameManager.Instance.StartGame();
    }
    public void LooseScreen()
    {
        if(nameInput.onValueChanged.GetPersistentEventCount() == 0)
            nameInput.onValueChanged.AddListener((delegate { AudioController.Instance.PlaySfx("Click"); }));

        nameInput.readOnly = false;
        nameInput.text = "";
        scoreTxt.gameObject.SetActive(false);
        loseScreen.SetActive(true);
        loseScore.text = "Score: " + GameManager.Instance.score.ToString();
        gameScreen.SetActive(false);
        playPauseButton.SetActive(false);

        //Play Add
        GoogleMobileAdsScript.Instance.RequestBanner();
    }

    public void RestartGame()
    {
        //Reset the game 
        for (int i = 0; i < ammoText.Length; i++)
        {
            ammoText[i].text = "";
        }

        AudioController.Instance.PlaySfx("DoubleClick");
        GameManager.Instance.ResetGame();
        scoreTxt.gameObject.SetActive(true);
        scoreTxt.text = "Score: 0";
        loseScore.text = "0";
        loseScreen.SetActive(false);
        gameScreen.SetActive(true);
        highScoreRecorded = false;

        //play pause screen
        playPauseButton.SetActive(true);
        playPauseScreen.SetActive(false);
        playPauseButton.GetComponent<TextMeshProUGUI>().text = "II";


        //Play Add
        GoogleMobileAdsScript.Instance.DestroyBanner();

        SceneManager.LoadScene(0);      
    }
}
