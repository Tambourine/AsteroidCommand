﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; 
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine.Events; 


public class GoogleMobileAdsScript : Singleton<GoogleMobileAdsScript>
{
    private BannerView bannerView;
    // Start is called before the first frame update

    void Start()
    {
        // Configure TagForChildDirectedTreatment and test device IDs.
        //RequestConfiguration requestConfiguration =
        //    new RequestConfiguration.Builder()
        //    .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.Unspecified)
        //    .SetTestDeviceIds(deviceIds).build();
        //MobileAds.SetRequestConfiguration(requestConfiguration);

        //Initialize+
        MobileAds.Initialize(init => { } );
        //this.RequestBanner(); 
    }
 
    public void RequestBanner()
    {
        #if UNITY_ANDROID
        string adUnitID = "ca-app-pub-3940256099942544/6300978111";
        //THE ACTUAL ONE FOR PUBLIC BUILD ca-app-pub-1013312584480634/6539471233
        // TEST ADDS ca-app-pub-3940256099942544/6300978111
#elif UNITY_IPHONE
        string adUnitID ="";
         //THE ACTUAL ONE FOR PUBLIC BUILD ca-app-pub-1013312584480634/4170254346
        // TEST ADS ca-app-pub-3940256099942544/2934735716
#else
        string adUnitID = "unexpected_platform";
#endif

        //CLEAN UP 
        if(bannerView != null)
        {
            bannerView.Destroy(); 
        }

        //Create Banner
        this.bannerView = new BannerView(adUnitID, AdSize.SmartBanner, AdPosition.Bottom);

        //add events 
        //this.bannerView.OnAdLoaded += this.HandleOnBannerLoaded;

        //create an add requset 
        AdRequest request = new AdRequest.Builder().Build();
        //Load the banner with the request
        this.bannerView.LoadAd(request);
    }

    public void HandleOnBannerLoaded(object sender, EventArgs args)
    {
        this.bannerView.Show();
    }

    public void DestroyBanner()
    {
        if (this.bannerView != null)
        {
            this.bannerView.Hide();
            this.bannerView.Destroy();
            Debug.Log("Destroyed Banner");
        }
    }

    private void OnDisable()
    {
        if(this.bannerView != null)
            this.bannerView.Destroy();


    }
}
